package com.zhao.nacoscontent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NacosUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacosUserApplication.class, args);
	}

}
