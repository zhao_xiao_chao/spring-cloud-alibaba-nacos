package com.zhao.nacoscontent;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class TestController {


    /**
     * 测试feign+nacos
     */
    @GetMapping("findUser")
    public String findUser() {
        return "zhaoxiaochao";
    }

}
