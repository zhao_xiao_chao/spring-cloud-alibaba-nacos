package com.zhao.nacoscontent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class NacosContentApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacosContentApplication.class, args);
	}

}
