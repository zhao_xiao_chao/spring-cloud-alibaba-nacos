package com.zhao.nacoscontent;

import com.zhao.nacoscontent.feignClient.UserCenterFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RefreshScope
public class TestController {
    @Autowired
    private DiscoveryClient discoveryClient;

    @Value("${my.configuration}")
    private String myConfiguration;

    @Autowired
    private UserCenterFeignClient userCenterFeignClient;


    /**
     * 测试：服务发现，内容中心通过nacos找到用户中心
     * @return 用户中心所有实例的地址信息
     */
    @GetMapping("findInstance")
    public List<ServiceInstance> getInstances() {
        // 查询指定服务的所有实例的信息
        return this.discoveryClient.getInstances("user-center");
    }
    /**
     * 测试获取配置参数
     */
    @GetMapping("getMyConfiguration")
    public String getMyConfiguration() {
        System.out.println(myConfiguration);
        return myConfiguration;
    }

    /**
     * 测试feign+nacos
     */
    @GetMapping("testFeignNacos")
    public String testFeignNacos() {
        System.out.println(userCenterFeignClient);
        return userCenterFeignClient.findUser();
    }

}
