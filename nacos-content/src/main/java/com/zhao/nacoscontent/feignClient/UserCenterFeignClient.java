package com.zhao.nacoscontent.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@Service
@FeignClient(name = "user-center")
public interface UserCenterFeignClient {

    @GetMapping("/findUser")
    String findUser();
}
